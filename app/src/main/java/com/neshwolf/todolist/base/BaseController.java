package com.neshwolf.todolist.base;

import android.view.LayoutInflater;
import android.view.View;

public abstract class BaseController<MODEL, VIEW extends BaseView<MODEL>>
    implements BaseView.ModelProvider<MODEL> {
  public VIEW view;
  public MODEL model;

  public BaseController(VIEW view, MODEL model) {
    this.view = view;
    this.model = model;
    connectCallbacksFromView(view);
    this.view.setModelProvider(this);
  }

  public abstract void connectCallbacksFromView(VIEW view);

  public void onPause() {

  }

  public void onResume() {

  }

  @Override public MODEL getModel() {
    return model;
  }

  public View getView(LayoutInflater layoutInflater) {
    return this.view.getView(layoutInflater);
  }
}
