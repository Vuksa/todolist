package com.neshwolf.todolist.base;

import android.view.LayoutInflater;
import android.view.View;

public abstract class BaseView<MODEL> {

  private ModelProvider<MODEL> modelProvider;
  private View view;

  public MODEL getModel() {
    return modelProvider.getModel();
  }

  public void rebindView() {
    bindModelToView(getModel());
  }

  public abstract View createView(LayoutInflater inflater);

  public abstract void bindModelToView(MODEL model);

  public final View getView(LayoutInflater layoutInflater) {
    if (view != null) {
      return view;
    }
    view = createView(layoutInflater);
    rebindView();
    connectViewToUI(view);
    return view;
  }

  public ModelProvider<MODEL> getModelProvider() {
    return modelProvider;
  }

  public void setModelProvider(ModelProvider<MODEL> modelProvider) {
    this.modelProvider = modelProvider;
  }

  protected abstract void connectViewToUI(View view);

  public interface ModelProvider<MODEL> {
    MODEL getModel();
  }
}
