package com.neshwolf.todolist.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import com.neshwolf.todolist.application.ApplicationComponent;
import com.neshwolf.todolist.application.ToDoApplication;

/**
 * Created by Vuksa on 18-Oct-16.
 */

public abstract class BaseActivity<MODEL, VIEW extends BaseView<MODEL>, CONTROLLER extends BaseController<MODEL, VIEW>, COMPONENT extends BaseComponent<VIEW, MODEL, CONTROLLER>>
    extends AppCompatActivity {

  private COMPONENT component;

  @Override public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
    super.onCreate(savedInstanceState, persistentState);
    connectCallbackFromController(getController());
    setContentView(getController().getView(LayoutInflater.from(this)));
  }

  protected abstract void connectCallbackFromController(CONTROLLER controller);

  public CONTROLLER getController() {
    return getComponent().getController();
  }

  @Override protected void onPause() {
    super.onPause();
    getController().onPause();
  }

  @Override protected void onResume() {
    super.onResume();
    getController().onResume();
  }

  public COMPONENT getComponent() {
    if (component == null) {
      component = createComponent();
    }
    return component;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    component = null;
  }

  protected abstract COMPONENT createComponent();

  public ApplicationComponent getApplicationComponent() {
    return ((ToDoApplication) getApplication()).getApplicationComponent();
  }
}
