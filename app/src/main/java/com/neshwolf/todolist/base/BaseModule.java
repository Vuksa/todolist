package com.neshwolf.todolist.base;

public interface BaseModule<MODEL, VIEW extends BaseView<MODEL>, CONTROLLER extends BaseController<MODEL, VIEW>> {

  MODEL providesModel();

  VIEW providesView();

  CONTROLLER providesController(MODEL model, VIEW view);
}
