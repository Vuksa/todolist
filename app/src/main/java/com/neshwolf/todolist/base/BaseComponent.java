package com.neshwolf.todolist.base;

/**
 * Created by Vuksa on 18-Oct-16.
 */

public interface BaseComponent<VIEW extends BaseView<MODEL>, MODEL, CONTROLLER extends BaseController<MODEL, VIEW>> {

  MODEL getModel();

  VIEW getView();

  CONTROLLER getController();
}
