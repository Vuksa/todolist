package com.neshwolf.todolist.application;

import com.neshwolf.todolist.storage.Storage;
import dagger.Component;
import javax.inject.Singleton;

@Singleton @Component(modules = ApplicationModule.class) public interface ApplicationComponent {

  Storage getStorage();
}
