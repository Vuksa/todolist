package com.neshwolf.todolist.application;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.neshwolf.todolist.storage.Storage;
import com.neshwolf.todolist.storage.StorageImpl;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module public class ApplicationModule {

  private static final String SHARED_PREF_NAME = "SIMPLE_SHARED_PREF_FOR_TO_DO_APP";
  private final Context context;

  public ApplicationModule(Context context) {
    this.context = context;
  }

  @Provides Context providesContext() {
    return context;
  }

  @Singleton @Provides SharedPreferences providesSharedPreferences() {
    return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
  }

  @Singleton @Provides Storage providesStorage(StorageImpl storage) {
    return storage;
  }

  @Singleton @Provides Gson providesGson() {
    return new Gson();
  }
}
