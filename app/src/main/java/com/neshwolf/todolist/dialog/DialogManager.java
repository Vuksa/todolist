package com.neshwolf.todolist.dialog;

public interface DialogManager {

  void showAddNewTaskDialog(DialogCallback callback);
}
