package com.neshwolf.todolist.dialog;

import android.content.Context;
import javax.inject.Inject;

public class DialogManagerImpl implements DialogManager {

  private final Context context;

  @Inject public DialogManagerImpl(Context context) {
    this.context = context;
  }

  @Override public void showAddNewTaskDialog(DialogCallback callback) {

  }
}
