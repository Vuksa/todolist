package com.neshwolf.todolist.dialog;

public interface DialogCallback {
  void OnCancelClicked();

  void onOkClicked();
}
