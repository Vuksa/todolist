package com.neshwolf.todolist;

import com.neshwolf.todolist.model.Task;
import java.util.List;

public class ToDoModel {
  List<Task> taskList;

  public ToDoModel(List<Task> taskList) {
    this.taskList = taskList;
  }

  public List<Task> getTaskList() {
    return taskList;
  }

  public void setTaskList(List<Task> taskList) {
    this.taskList = taskList;
  }
}
