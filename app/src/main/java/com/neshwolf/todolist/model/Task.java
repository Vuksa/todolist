package com.neshwolf.todolist.model;

public class Task {

  protected String title;
  protected String description;
  protected boolean isTaskFinished;

  public Task(String title, String description, boolean isTaskFinished) {
    this.title = title;
    this.description = description;
    this.isTaskFinished = isTaskFinished;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isTaskFinished() {
    return isTaskFinished;
  }

  public void setTaskFinished(boolean taskFinished) {
    isTaskFinished = taskFinished;
  }
}
