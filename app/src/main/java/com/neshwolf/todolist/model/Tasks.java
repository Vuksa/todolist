package com.neshwolf.todolist.model;

import java.util.List;

public class Tasks {
  List<Task> tasks;

  public Tasks(List<Task> tasks) {
    this.tasks = tasks;
  }

  public List<Task> getTasks() {
    return tasks;
  }

  public void setTasks(List<Task> tasks) {
    this.tasks = tasks;
  }
}
