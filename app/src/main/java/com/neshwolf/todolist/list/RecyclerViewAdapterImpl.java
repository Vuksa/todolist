package com.neshwolf.todolist.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.neshwolf.todolist.R;
import com.neshwolf.todolist.ToDoModel;
import com.neshwolf.todolist.base.BaseView;
import com.neshwolf.todolist.list.viewholder.TaskItemViewHolder;
import com.neshwolf.todolist.model.Task;
import javax.inject.Inject;

public class RecyclerViewAdapterImpl extends RecyclerViewAdapter
    implements TaskItemViewHolder.TaskItemViewHolderCallback {

  private RecyclerViewAdapterCallback callback;
  private BaseView.ModelProvider<ToDoModel> modelProvider;

  @Inject public RecyclerViewAdapterImpl() {
  }

  @Override public void setCallback(RecyclerViewAdapterCallback callback) {
    this.callback = callback;
  }

  @Override public void setModelProvider(BaseView.ModelProvider<ToDoModel> modelProvider) {
    this.modelProvider = modelProvider;
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.task_viewholder_layout, null, false);
    return new TaskItemViewHolder(view);
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    Task task = getItem(position);
    if (holder instanceof TaskItemViewHolder) {
      ((TaskItemViewHolder) holder).bindData(task, this);
    }
  }

  private Task getItem(int position) {
    return modelProvider.getModel().getTaskList().get(position);
  }

  @Override public int getItemCount() {
    return modelProvider.getModel().getTaskList().size();
  }

  @Override public void onTaskClicked(Task task) {
    callback.onTaskClicked(task);
  }
}
