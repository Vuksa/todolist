package com.neshwolf.todolist.list.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import com.hanks.library.AnimateCheckBox;
import com.neshwolf.todolist.R;
import com.neshwolf.todolist.model.Task;

public class TaskItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

  @BindView(R.id.task_title) TextView taskTitle;
  @BindView(R.id.task_finished_checkbox) AnimateCheckBox taskFinishedCheckbox;
  private TaskItemViewHolderCallback callback;
  private Task task;

  public TaskItemViewHolder(View itemView) {
    super(itemView);
    itemView.setOnClickListener(this);
  }

  public void bindData(Task task, TaskItemViewHolderCallback callback) {
    this.task = task;
    this.callback = callback;
    taskTitle.setText(task.getTitle());
    taskFinishedCheckbox.setChecked(task.isTaskFinished() ? true : false);
  }

  @Override public void onClick(View v) {
    if (callback == null) return;
    callback.onTaskClicked(task);
  }

  public interface TaskItemViewHolderCallback {
    void onTaskClicked(Task task);
  }
}
