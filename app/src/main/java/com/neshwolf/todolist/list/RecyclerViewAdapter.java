package com.neshwolf.todolist.list;

import android.support.v7.widget.RecyclerView;
import com.neshwolf.todolist.ToDoModel;
import com.neshwolf.todolist.base.BaseView;
import com.neshwolf.todolist.model.Task;

public abstract class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  public abstract void setCallback(RecyclerViewAdapterCallback callback);

  public abstract void setModelProvider(BaseView.ModelProvider<ToDoModel> modelProvider);

  public interface RecyclerViewAdapterCallback {
    void onTaskClicked(Task task);
  }
}
