package com.neshwolf.todolist;

import com.neshwolf.todolist.base.BaseActivity;

public class MainActivity extends BaseActivity<ToDoModel, ToDoView, ToDoController, ToDoComponent>
    implements ToDoController.ToDoControllerCallback {

  @Override protected void connectCallbackFromController(ToDoController controller) {
    controller.setCallback(this);
  }

  @Override protected ToDoComponent createComponent() {
    return DaggerToDoComponent.builder()
        .toDoModule(new ToDoModule(getBaseContext(), getApplicationComponent()))
        .build();
  }
}
