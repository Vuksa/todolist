package com.neshwolf.todolist;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.neshwolf.todolist.base.BaseView;
import com.neshwolf.todolist.list.RecyclerViewAdapter;
import com.neshwolf.todolist.model.Task;

public class ToDoView extends BaseView<ToDoModel>
    implements View.OnClickListener, RecyclerViewAdapter.RecyclerViewAdapterCallback {

  private final LinearLayoutManager layoutManager;
  private final RecyclerViewAdapter adapter;
  @BindView(R.id.fab) FloatingActionButton fab;
  @BindView(R.id.todolist) RecyclerView task_recyclerview;
  private ToDoViewCallback callback;

  public ToDoView(LinearLayoutManager layoutManager, RecyclerViewAdapter adapter) {
    this.layoutManager = layoutManager;
    this.adapter = adapter;
  }

  @Override public View createView(LayoutInflater inflater) {
    View view = inflater.inflate(R.layout.activity_main, null, true);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override public void bindModelToView(ToDoModel toDoModel) {

  }

  @Override protected void connectViewToUI(View view) {
    adapter.setCallback(this);
    adapter.setModelProvider(getModelProvider());
    task_recyclerview.setAdapter(adapter);
    task_recyclerview.setLayoutManager(layoutManager);
    fab.setOnClickListener(this);
  }

  public void setCallback(ToDoViewCallback callback) {
    this.callback = callback;
  }

  @Override public void onClick(View v) {
    if (callback == null) return;
    if (v.getId() == fab.getId()) {
      callback.clickedOnAddNewTask();
    } else {
      throw new RuntimeException("Unhandled click on view in " + getClass().getSimpleName());
    }
  }

  @Override public void onTaskClicked(Task task) {
    if (callback == null) return;
    callback.clickedOnTask(task);
  }

  public interface ToDoViewCallback {
    void clickedOnAddNewTask();

    void clickedOnTask(Task task);
  }
}
