package com.neshwolf.todolist;

import com.neshwolf.todolist.base.BaseController;
import com.neshwolf.todolist.dialog.DialogCallback;
import com.neshwolf.todolist.dialog.DialogManager;
import com.neshwolf.todolist.model.Task;
import com.neshwolf.todolist.storage.StorageCallback;
import com.neshwolf.todolist.storage.StorageError;
import com.neshwolf.todolist.storage.StorageOperation;

public class ToDoController extends BaseController<ToDoModel, ToDoView>
    implements ToDoView.ToDoViewCallback, DialogCallback,
    StorageCallback<StorageOperation, StorageError> {

  private final DialogManager dialogManager;
  private ToDoControllerCallback callback;

  public ToDoController(ToDoView view, ToDoModel toDoModel, DialogManager dialogManager) {
    super(view, toDoModel);
    this.dialogManager = dialogManager;
  }

  @Override public void connectCallbacksFromView(ToDoView view) {
    view.setCallback(this);
  }

  public void setCallback(ToDoControllerCallback callback) {
    this.callback = callback;
  }

  @Override public void clickedOnAddNewTask() {
    if (dialogManager == null) return;
    dialogManager.showAddNewTaskDialog(this);
  }

  @Override public void clickedOnTask(Task task) {

  }

  @Override public void OnCancelClicked() {

  }

  @Override public void onOkClicked() {

  }

  @Override public void onSuccess(StorageOperation storageOperation) {
  }

  @Override public void onError(StorageError storageError) {

  }

  public interface ToDoControllerCallback {

  }
}
