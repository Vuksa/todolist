package com.neshwolf.todolist.storage;

public interface StorageCallback<OPERATION_TYPE, ERROR_TYPE> {

  void onSuccess(OPERATION_TYPE operation_type);

  void onError(ERROR_TYPE error_type);
}
