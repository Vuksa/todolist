package com.neshwolf.todolist.storage;

public enum StorageOperation {
  ITEM_UPDATE, ITEM_ADD, ITEM_DELETE
}
