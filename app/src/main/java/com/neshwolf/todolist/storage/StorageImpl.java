package com.neshwolf.todolist.storage;

import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.neshwolf.todolist.model.Task;
import com.neshwolf.todolist.model.Tasks;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class StorageImpl implements Storage {

  private static final String LIST_OF_TASKS = "LIST_OF_TASKS";
  private final SharedPreferences preferences;
  private final Gson gson;
  private List<Task> tasks;

  @Inject StorageImpl(SharedPreferences preferences, Gson gson) {
    this.preferences = preferences;
    this.gson = gson;
  }

  @Override public void saveNewTask(Task task, StorageCallback callback) {
    tasks = getAllTasks();
    if (checkIfTaskWithTitleExists(task, tasks)) {
      callback.onError(StorageError.TASK_ALREADY_EXISTS);
      return;
    }
    tasks.add(task);
    boolean success = preferences.edit().putString(LIST_OF_TASKS, gson.toJson(tasks)).commit();
    if (success) {
      callback.onSuccess(StorageOperation.ITEM_ADD);
    } else {
      callback.onError(StorageError.ERROR_WHILE_SAVING_TASK);
    }
  }

  @Override public void updateTask(Task task, StorageCallback callback) {
    tasks = getAllTasks();
    Task t = getTask(tasks, task.getTitle());
    if (t == null) {
      callback.onError(StorageError.TASK_DOES_NOT_EXIST);
    } else {
      tasks.remove(t);
      t.setTaskFinished(task.isTaskFinished());
      tasks.add(t);
      boolean success = preferences.edit().putString(LIST_OF_TASKS, gson.toJson(tasks)).commit();
      t = null;
      tasks = null;
      if (success) {
        callback.onSuccess(StorageOperation.ITEM_UPDATE);
      } else {
        callback.onError(StorageError.ERROR_WHILE_UPDATING_TASK);
      }
    }
  }

  @Override public void deleteTask(Task task, StorageCallback callback) {
    if (getAllTasks().remove(task)) {
      callback.onSuccess(StorageOperation.ITEM_DELETE);
    } else {
      callback.onError(StorageError.ERROR_ON_DELETING_ITEM);
    }
  }

  @Override public List<Task> getAllTasks() {
    Tasks tasks = gson.fromJson(preferences.getString(LIST_OF_TASKS, ""), Tasks.class);
    if (tasks.getTasks() == null || tasks.getTasks().size() == 0) return getDummyList();

    return tasks.getTasks();
  }

  private boolean checkIfTaskWithTitleExists(Task task, List<Task> tasks) {
    for (Task t : tasks) {
      if (t.getTitle().equalsIgnoreCase(task.getTitle())) return true;
    }
    return false;
  }

  private Task getTask(List<Task> tasks, String title) {
    for (Task t : tasks) {
      if (title.equalsIgnoreCase(t.getTitle())) return t;
    }
    return null;
  }

  public List<Task> getDummyList() {
    List tasks = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      tasks.add(new Task("Task " + i, "This task is " + i + ". to be solved.",
          i % 2 == 0 ? true : false));
    }
    return tasks;
  }
}
