package com.neshwolf.todolist.storage;

import com.neshwolf.todolist.model.Task;
import java.util.List;

public interface Storage {

  void saveNewTask(Task task, StorageCallback callback);

  void updateTask(Task task, StorageCallback callback);

  void deleteTask(Task task, StorageCallback callback);

  List<Task> getAllTasks();
}
