package com.neshwolf.todolist;

import com.neshwolf.todolist.base.BaseComponent;
import dagger.Component;

@ToDoListScope @Component(modules = ToDoModule.class) public interface ToDoComponent
    extends BaseComponent<ToDoView, ToDoModel, ToDoController> {
}
