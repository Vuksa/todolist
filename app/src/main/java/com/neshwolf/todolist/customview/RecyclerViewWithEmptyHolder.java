package com.neshwolf.todolist.customview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class RecyclerViewWithEmptyHolder extends RecyclerView {

  private final CustomAdapterObserver observer;
  private View emptyViewHolder;
  private boolean isRefreshing;

  public RecyclerViewWithEmptyHolder(Context context) {
    super(context);
    observer = new CustomAdapterObserver();
  }

  public RecyclerViewWithEmptyHolder(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    observer = new CustomAdapterObserver();
  }

  public RecyclerViewWithEmptyHolder(Context context, @Nullable AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    observer = new CustomAdapterObserver();
  }

  private boolean isEmptyViewInitialized() {
    return emptyViewHolder != null && getAdapter() != null;
  }

  public void setRefreshing(boolean refreshing) {
    isRefreshing = refreshing;
  }

  public void checkIfRecyclerViewIsEmpty() {
    if (isEmptyViewInitialized()) {
      if (!isRefreshing) {
        boolean emptyViewVisible = getAdapter().getItemCount() == 1;
        if (emptyViewHolder != null) {
          emptyViewHolder.setVisibility(emptyViewVisible ? VISIBLE : GONE);
        }
        setVisibility(emptyViewVisible ? GONE : VISIBLE);
      }
    }
  }

  public void setAdapter(Adapter adapter) {
    final Adapter oldAdapter = getAdapter();
    if (oldAdapter != null) {
      oldAdapter.unregisterAdapterDataObserver(observer);
    }
    super.setAdapter(adapter);
    if (adapter != null) {
      adapter.registerAdapterDataObserver(observer);
    }
    checkIfRecyclerViewIsEmpty();
  }

  public void setEmptyViewHolder(View emptyViewHolder) {
    this.emptyViewHolder = emptyViewHolder;
    checkIfRecyclerViewIsEmpty();
  }

  public class CustomAdapterObserver extends AdapterDataObserver {
    @Override public void onChanged() {
      checkIfRecyclerViewIsEmpty();
    }

    @Override public void onItemRangeInserted(int positionStart, int itemCount) {
      checkIfRecyclerViewIsEmpty();
    }

    @Override public void onItemRangeRemoved(int positionStart, int itemCount) {
      checkIfRecyclerViewIsEmpty();
    }
  }
}
