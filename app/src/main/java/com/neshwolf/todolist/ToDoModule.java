package com.neshwolf.todolist;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import com.neshwolf.todolist.application.ApplicationComponent;
import com.neshwolf.todolist.dialog.DialogManager;
import com.neshwolf.todolist.dialog.DialogManagerImpl;
import com.neshwolf.todolist.list.RecyclerViewAdapter;
import com.neshwolf.todolist.list.RecyclerViewAdapterImpl;
import com.neshwolf.todolist.storage.Storage;
import dagger.Module;
import dagger.Provides;

@Module public class ToDoModule {

  private final ApplicationComponent applicationComponent;
  private final Context context;

  public ToDoModule(Context context, ApplicationComponent applicationComponent) {
    this.applicationComponent = applicationComponent;
    this.context = context;
  }

  @ToDoListScope @Provides public ToDoModel providesModel(Storage storage) {
    return new ToDoModel(storage.getAllTasks());
  }

  @ToDoListScope @Provides
  public ToDoView providesView(RecyclerViewAdapter adapter, LinearLayoutManager layoutManager) {
    return new ToDoView(layoutManager, adapter);
  }

  @ToDoListScope @Provides public ToDoController providesController(ToDoModel model, ToDoView view,
      DialogManager dialogManager) {
    return new ToDoController(view, model, dialogManager);
  }

  @Provides public DialogManager providesDialogManager(DialogManagerImpl dialogManager) {
    return dialogManager;
  }

  @Provides public Storage providesStorage() {
    return applicationComponent.getStorage();
  }

  @Provides public LinearLayoutManager providesLinearLayoutManager() {
    return new LinearLayoutManager(context);
  }

  @Provides public RecyclerViewAdapter providesRecyclerViewAdapter(RecyclerViewAdapterImpl impl) {
    return impl;
  }
}
